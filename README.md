# React Text Adventure Node Editor

Graphical node editor to go along with [React Text Adventure](https://gitlab.com/Acepie/React-Text-Adventure). Allows for adding, editing, and deleting nodes/edges.
