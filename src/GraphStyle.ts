export default {
  layout: { fit: true },
  style: [
    // the stylesheet for the graph
    {
      selector: "node",
      style: {
        "background-color": "#dddddd",
        content: "data(title)",
        height: "150",
        shape: "roundrectangle",
        "text-halign": "center",
        "text-max-width": "150",
        "text-valign": "center",
        "text-wrap": "wrap",
        width: "150"
      }
    },
    {
      selector: "edge",
      style: {
        "curve-style": "bezier",
        label: "data(text)",
        "line-color": "#ccc",
        "target-arrow-shape": "triangle",
        width: 3
      }
    }
  ]
};
