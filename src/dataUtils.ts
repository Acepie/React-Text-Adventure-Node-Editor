import { EdgeCollection, ElementDefinition, NodeCollection } from "cytoscape";

export function elementsToJson(nodes: NodeCollection, edges: EdgeCollection) {
  const retnodes = nodes.map(node => {
    return { ...node.data() };
  });

  const idMap = retnodes.reduce((acc, node, i) => {
    return { ...acc, [node.id]: i };
  }, {});

  edges.forEach(edge => {
    const edgeData = edge.data();
    const sourceNode = retnodes[idMap[edgeData.source]];
    if (!sourceNode.choices) {
      sourceNode.choices = [];
    }

    const data = { ...edgeData };
    data.id = idMap[data.target];
    delete data.source;
    delete data.target;
    sourceNode.choices.push(data);
  });

  return retnodes.map(node => {
    const { id, ...rest } = node;
    return { ...rest };
  });
}

export function jsonToElements(nodes): ElementDefinition[] {
  const retelements = [];
  nodes.forEach((node, i) => {
    const element = { ...node, id: i };
    if (element.choices) {
      element.choices.forEach(choice => {
        retelements.push({
          data: {
            ...choice,
            id: "edge" + String(i) + String(choice.id),
            source: i,
            target: choice.id
          }
        });
      });
    }
    delete element.choices;
    retelements.push({
      data: element
    });
  });

  return retelements;
}
