import * as React from "react";
import * as ReactDom from "react-dom";
import { jsonToElements } from "./dataUtils";
import GraphTools from "./GraphTools";

const sampleElements = jsonToElements([
  {
    choices: [
      {
        id: 1,
        text: "The performer?"
      }
    ],
    text:
      "Oh hello, I'm glad you could make it tonight. I've heard some really good things about the performer",
    title: "The Show"
  },
  {
    choices: [
      {
        disabledText: "I seem to be missing a ticket",
        id: 2,
        requiredItems: {
          Ticket: 1
        },
        text: "Let's get going"
      },
      {
        id: 3,
        text: "Give me a minute"
      }
    ],
    text:
      "Yes, a lovely young pianist from the city. Are you ready to go in? The performance is about to begin.",
    title: "The Performer"
  },
  {
    choices: [
      {
        id: 3,
        text: "Actually I need a minute"
      }
    ],
    text: "Well these are lovely seats. I'm so excited for the show to start.",
    title: "Seating"
  },
  {
    choices: [
      {
        disabledText: "I seem to be missing a ticket",
        id: 2,
        requiredItems: {
          Ticket: 1
        },
        text: "Let's get going"
      },
      {
        disabledText: "I don't have enough money",
        id: 4,
        requiredItems: {
          Coin: 500
        },
        text: "Buy a ticket"
      }
    ],
    text: "Alright don't be too long",
    title: "The Lobby"
  },
  {
    acquireOnEnter: {
      Ticket: 1
    },
    choices: [
      {
        disabledText: "I seem to be missing a ticket",
        id: 2,
        requiredItems: {
          Ticket: 1
        },
        text: "Let's get going"
      }
    ],
    loseOnEnter: {
      Coin: 500
    },
    text: "Excellent choice. Enjoy the show!",
    title: "Buying a ticket"
  }
]);

ReactDom.render(
  <GraphTools elements={sampleElements} />,
  document.getElementById("root")
);
