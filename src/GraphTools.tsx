import cytoscape, { Core, ElementDefinition } from "cytoscape";
import * as React from "react";
import { elementsToJson, jsonToElements } from "./dataUtils";
import GraphStyle from "./GraphStyle";
import SidePanel from "./SidePanel";

interface GraphToolsProps {
  elements: ElementDefinition[];
}

interface GraphToolsState {
  activeElement?: any;
  containerRef?: HTMLDivElement;
  cy?: Core;
  addingEdge: boolean;
}

export default class GraphTools extends React.Component<
  GraphToolsProps,
  GraphToolsState
> {
  constructor(props: GraphToolsProps) {
    super(props);
    this.state = {
      activeElement: null,
      addingEdge: false,
      containerRef: null,
      cy: null
    };
  }

  public loadCy = (elements: any[]) => {
    const cy = cytoscape({
      container: this.state.containerRef,
      elements,
      ...GraphStyle
    } as any);

    cy.on("tap", "node", ({ target }) => {
      const data = target.data();
      const { activeElement, addingEdge } = this.state;

      if (addingEdge) {
        const {
          activeElement: { id: src }
        } = this.state;
        const dest = data.id;

        const id = `edge${src}${dest}`;

        const newEdge = { data: { id, text: "", source: src, target: dest } };

        cy.add(newEdge as any);

        this.setState({ addingEdge: false });
      }

      this.setState({
        activeElement:
          !activeElement || activeElement.id !== data.id ? data : null
      });
    });

    cy.on("tap", "edge", ({ target }) => {
      const data = target.data();
      const { activeElement } = this.state;

      this.setState({
        activeElement:
          !activeElement || activeElement.id !== data.id ? data : null
      });
    });

    return cy;
  };

  public componentDidUpdate(_: GraphToolsProps, prevState: GraphToolsState) {
    if (prevState.containerRef === null && this.state.containerRef !== null) {
      this.setState({ cy: this.loadCy(this.props.elements) });
    }
  }

  public getExport() {
    const { cy } = this.state;
    if (!cy) {
      return "";
    }

    return `data:application/json;charset=utf-8,${encodeURIComponent(
      JSON.stringify(elementsToJson(cy.nodes(), cy.edges()))
    )}`;
  }

  public importFile = (file: File) => {
    if (!file.name.match(/(\.json)$/)) {
      return;
    }

    const reader = new FileReader();
    reader.onload = res => {
      try {
        const data = JSON.parse(res.target.result);
        const elements = jsonToElements(data);
        this.state.cy.destroy();
        this.setState({ cy: this.loadCy(elements) });
      } catch (e) {
        alert("Failed to load json data. JSON may be formatted incorrectly");
      }
    };

    reader.readAsText(file);
  };

  public addNode = () => {
    const { cy } = this.state;
    const id = cy.nodes().reduce((acc, node) => {
      const nid = Number(node.data("id"));
      return nid >= acc ? nid + 1 : acc;
    }, 0);

    const newNode = {
      data: { id, title: "", text: "" }
    };

    cy.add(newNode as any);
    cy.layout({ name: "circle", fit: true }).run();
  };

  public addEdge = () => {
    this.setState({ addingEdge: true });
  };

  public saveElement = (node: any, title: string, text: string) => {
    node.title = title;
    node.text = text;
  };

  public deleteNode = () => {
    const { cy, activeElement } = this.state;
    const deadEdges = cy.edges().filter(edge => {
      return (
        edge.data("source") === activeElement.id ||
        edge.data("target") === activeElement.id
      );
    });

    cy.batch(() => {
      cy.remove(`#${activeElement.id}`);
      cy.remove(deadEdges);
    });
    this.setState({
      activeElement: null
    });
  };

  public deleteEdge = () => {
    const { cy, activeElement } = this.state;
    cy.remove(`#${activeElement.id}`);
  };

  public render() {
    const { activeElement, addingEdge } = this.state;
    return (
      <div className="graph">
        <div
          id="cy"
          ref={elt => {
            if (!this.state.containerRef) {
              this.setState({ containerRef: elt });
            }
          }}
        />
        <SidePanel
          element={activeElement}
          exportLink={this.getExport()}
          addingEdge={addingEdge}
          addEdge={this.addEdge}
          addNode={this.addNode}
          saveElement={this.saveElement}
          deleteEdge={this.deleteEdge}
          deleteNode={this.deleteNode}
          importFile={this.importFile}
        />
      </div>
    );
  }
}
