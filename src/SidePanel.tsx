import * as React from "react";

interface SidePanelProps {
  element?: any;
  exportLink: string;
  addingEdge: boolean;
  addEdge(): void;
  addNode(): void;
  saveElement(element: any, title: string, text: string): void;
  deleteEdge(): void;
  deleteNode(): void;
  importFile(file: File): void;
}

interface SidePanelState {
  editing: boolean;
  title: string;
  text: string;
}

function editingEdge(id: string) {
  return id.includes("edge");
}

export default class SidePanel extends React.Component<
  SidePanelProps,
  SidePanelState
> {
  constructor(props: SidePanelProps) {
    super(props);
    this.state = { editing: false, title: "", text: "" };
  }

  public renderNodeData() {
    const { element } = this.props;

    if (editingEdge(element.id)) {
      return (
        <div className="node-data">
          <h2>Choice Label: {element.text}</h2>
          <p>ID: {element.id}</p>
        </div>
      );
    }

    return (
      <div className="node-data">
        <h2>Title: {element.title}</h2>
        <p>Text: {element.text}</p>
        <p>ID: {element.id}</p>
      </div>
    );
  }

  public renderEditForm() {
    const { element, saveElement } = this.props;
    const { title, text } = this.state;
    return (
      <form id="edit-node">
        {editingEdge(element.id) ? null : (
          <label>
            Title
            <input
              type="text"
              name="Title"
              id="EditTitle"
              value={title}
              onChange={e => {
                this.setState({ title: e.target.value });
              }}
            />
          </label>
        )}
        <label>
          Text
          <input
            type="text"
            name="Text"
            id="EditText"
            value={text}
            onChange={e => {
              this.setState({ text: e.target.value });
            }}
          />
        </label>
        <button
          id="update"
          onClick={e => {
            e.preventDefault();
            saveElement(element, title, text);
            this.setState({ editing: false, text: "", title: "" });
          }}
        >
          Finish
        </button>
      </form>
    );
  }

  public renderExport() {
    const { exportLink } = this.props;
    return (
      <a download="story-data.json" href={exportLink}>
        <button>Export</button>
      </a>
    );
  }

  public renderAdd() {
    return (
      <button
        id="add"
        onClick={e => {
          e.preventDefault();
          this.props.addNode();
        }}
      >
        Add
      </button>
    );
  }

  public render() {
    const {
      element,
      addEdge,
      deleteEdge,
      deleteNode,
      importFile,
      addingEdge
    } = this.props;
    const { editing } = this.state;

    if (!element) {
      return (
        <aside>
          {this.renderExport()}
          <label className="import">
            <input
              id="import"
              type="file"
              onChange={e => {
                const file = e.target.files[0];
                if (file) {
                  importFile(file);
                }
              }}
            />
            Import
          </label>
          {this.renderAdd()}
        </aside>
      );
    }
    return (
      <aside>
        {this.renderExport()}
        {addingEdge ? <h3>Click on a node to add an edge</h3> : null}
        {this.renderNodeData()}
        {!editing && !editingEdge(element.id) ? (
          <button
            id="addEdge"
            onClick={e => {
              e.preventDefault();
              addEdge();
            }}
          >
            Add edge
          </button>
        ) : null}
        {!editing ? (
          <button
            id="delete"
            onClick={e => {
              e.preventDefault();
              editingEdge(element.id) ? deleteEdge() : deleteNode();
            }}
          >
            Delete
          </button>
        ) : null}
        {editing ? (
          this.renderEditForm()
        ) : (
          <button
            id="edit"
            onClick={() => {
              this.setState({
                editing: true,
                text: element.text,
                title: element.title
              });
            }}
          >
            Edit
          </button>
        )}
      </aside>
    );
  }
}
